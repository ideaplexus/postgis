#!make

POSTGRES_VERSION=17
BUILD_CACHE_PREFIX=
PROXY_CACHE_PREFIX=

.PHONY: help

help: ## Show this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\033[36m\033[0m\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-20s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

update: ## Update Dockerfiles based on config
	go mod tidy -v
	go get -v -t -u ./...
	go mod vendor -v
	go run update.go

proj: ## Build proj
	docker build \
		--file dist/Dockerfile.proj \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-proj" \
		.

cgal: ## Build cgal
	docker build \
		--file dist/Dockerfile.cgal \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-cgal" \
		.

geos: ## Build geos
	docker build \
		--file dist/Dockerfile.geos \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-geos" \
		.

libpqxx: ## Build libpqxx
	docker build \
		--file dist/Dockerfile.libpqxx \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-libpqxx:postgres-$(POSTGRES_VERSION)" \
		.

sfcgal: cgal ## Build sfcgal
	docker build \
		--file dist/Dockerfile.sfcgal \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-sfcgal" \
		.

gdal: proj geos cgal sfcgal ## Build gdal
	docker build \
		--file dist/Dockerfile.gdal \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-gdal:postgres-$(POSTGRES_VERSION)" \
		.

postgis: proj geos cgal sfcgal gdal ## Build postgis
	docker build \
		--file dist/Dockerfile.postgis \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-postgis:postgres-$(POSTGRES_VERSION)" \
		.

pgrouting: libpqxx ## Build pgrouting
	docker build \
		--file dist/Dockerfile.pgrouting \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag $(PROXY_CACHE_PREFIX)postgis-pgrouting:postgres-$(POSTGRES_VERSION) \
		.

osm2pgsql: proj ## Build osm2pgsql
	docker build \
		--file dist/Dockerfile.osm2pgsql \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-osm2pgsql:postgres-$(POSTGRES_VERSION)" \
		.

osm2pgrouting: libpqxx ## Build osm2pgrouting
	docker build \
		--file dist/Dockerfile.osm2pgrouting \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)postgis-osm2pgrouting:postgres-$(POSTGRES_VERSION)" \
		.

all: proj cgal sfcgal geos gdal libpqxx postgis pgrouting osm2pgsql osm2pgrouting ## Build final container
	docker build \
		--file dist/Dockerfile.all \
		--build-arg POSTGRES_VERSION=$(POSTGRES_VERSION) \
		--build-arg PROXY_CACHE_PREFIX=$(PROXY_CACHE_PREFIX) \
		--build-arg BUILD_CACHE_PREFIX=$(BUILD_CACHE_PREFIX) \
		--tag "$(PROXY_CACHE_PREFIX)ideaplexus/postgis:postgres-$(POSTGRES_VERSION)" \
		--tag "$(PROXY_CACHE_PREFIX)ideaplexus/postgis:latest" \
		.
