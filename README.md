# POSTGIS

This [repository](https://gitlab.com/ideaplexus/postgis) contains the source code for the docker image `ideaplexus/postgis`.

These docker images extending the original `PostgreSQL` images with `PostGIS`, `PGRouting` and some other useful tools.

## Usage

Since this image is created on top of the PostgreSQL docker image, please refer to the original [documentation](https://github.com/docker-library/docs/blob/master/postgres/README.md).
Note that the original entrypoint file (`/docker-entrypoint-initdb.d/postgis.sh`)
In addition, there is the `/usr/local/bin/update-postgis.sh`, which can be used to update an existing PostgreSQL database with the latest extension versions.

## Alpine version

- 3.21

## PostgreSQL versions

- 17
- 16
- 15
- 14
- 13

## Package versions

- **libpqxx** 7.10.0
- **geos** 3.13.0
- **gdal** 3.10.1
- **cgal** 6.0.1
- **sfcgal** 2.0.0
- **proj4** 9.5.1
- **postgis** 3.5.2
- **pgrouting** 3.7.3
- **osm2pgsql** 2.0.1
- **osm2pgrouting** 2.3.8
