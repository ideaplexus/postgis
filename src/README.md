# POSTGIS

This [repository](https://gitlab.com/ideaplexus/postgis) contains the source code for the docker image `ideaplexus/postgis`.

These docker images extending the original `PostgreSQL` images with `PostGIS`, `PGRouting` and some other useful tools.

## Usage

Since this image is created on top of the PostgreSQL docker image, please refer to the original [documentation](https://github.com/docker-library/docs/blob/master/postgres/README.md).
Note that the original entrypoint file (`/docker-entrypoint-initdb.d/postgis.sh`)
In addition, there is the `/usr/local/bin/update-postgis.sh`, which can be used to update an existing PostgreSQL database with the latest extension versions.

## Alpine version

- {{ .Alpine }}

## PostgreSQL versions
{{ range $version := .Postgres }}
- {{ $version -}}
{{ end }}

## Package versions

- **libpqxx** {{ .Sources.libpqxx.Version }}
- **geos** {{ .Sources.geos.Version }}
- **gdal** {{ .Sources.gdal.Version }}
- **cgal** {{ .Sources.cgal.Version }}
- **sfcgal** {{ .Sources.sfcgal.Version }}
- **proj4** {{ .Sources.proj.Version }}
- **postgis** {{ .Sources.postgis.Version }}
- **pgrouting** {{ .Sources.pgrouting.Version }}
- **osm2pgsql** {{ .Sources.osm2pgsql.Version }}
- **osm2pgrouting** {{ .Sources.osm2pgrouting.Version }}
