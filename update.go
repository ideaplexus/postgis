package main

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"text/template"

	"crypto/sha256"

	"github.com/charmbracelet/log"
	"golang.org/x/sync/errgroup"
	yaml "gopkg.in/yaml.v3"
)

type Source struct {
	Version   string `yaml:"version"`
	URL       string `yaml:"url"`
	Template  string `yaml:"template`
	SHA256Sum string `yaml:"-"`
}

type Config struct {
	Sources  map[string]*Source `yaml:"sources"`
	Postgres []string           `yaml:"postgres"`
	Alpine   string             `yaml:"alpine"`
}

const (
	DefaultFilePerm = 0o644
	DefaultDirPerm  = 0o755
)

func main() {
	config := loadConfig()
	prepare(config)
	render(config)
	renderReadme(config)
	renderGitlabCIFile(config)
}

func loadConfig() *Config {
	file, err := os.ReadFile("config.yaml")
	if err != nil {
		panic(err)
	}

	config := Config{}
	err = yaml.Unmarshal(file, &config)
	if err != nil {
		panic(err)
	}

	return &config
}

func download(url string, file string) error {
	response, err := http.Get(url)
	if err != nil {
		return err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return fmt.Errorf("bad status: %s", response.Status)
	}

	out, err := os.Create(file)
	if err != nil {
		return err
	}

	defer out.Close()

	_, err = io.Copy(out, response.Body)
	if err != nil {
		return err
	}

	return nil
}

func prepare(config *Config) error {
	log.Info("Fetch sources")

	g := new(errgroup.Group)

	err := os.MkdirAll("./cache", DefaultDirPerm)
	if err != nil {
		return err
	}

	for name, source := range config.Sources {
		name := name
		source := source

		g.Go(func() error {
			fileName := fmt.Sprintf("./cache/%s-%s.tar.gz", name, source.Version)

			if _, err := os.Stat(fileName); errors.Is(err, os.ErrNotExist) {
				err := download(strings.Replace(source.URL, "%VERSION", source.Version, -1), fileName)
				if err != nil {
					return err
				}
			}

			bytes, err := os.ReadFile(fileName)
			if err != nil {
				return err
			}

			source.SHA256Sum = fmt.Sprintf("%x", sha256.Sum256(bytes))

			return nil
		})
	}

	if err := g.Wait(); err == nil {
		log.Info("Successfully fetched sources")
	}

	return nil
}

func render(config *Config) {
	log.Info("Render sources")

	g := new(errgroup.Group)

	for _, source := range config.Sources {
		source := source

		g.Go(func() error {
			return renderTemplate(source.Template, config)
		})
	}

	g.Go(func() error {
		return renderTemplate("Dockerfile.all", config)
	})

	if err := g.Wait(); err == nil {
		log.Info("Successfully rendered sources")
	}
}

func renderTemplate(name string, config *Config) error {
	file, err := os.OpenFile(fmt.Sprintf("./dist/%s", name), os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
	if err != nil {
		return err
	}

	defer file.Close()

	tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/%s", name)))
	err = tmpl.Execute(file, *config)
	if err != nil {
		return err
	}

	return nil
}

func renderReadme(config *Config) error {
	name := "README.md"
	file, err := os.OpenFile(fmt.Sprintf("./%s", name), os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
	if err != nil {
		return err
	}

	defer file.Close()

	tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/%s", name)))
	err = tmpl.Execute(file, *config)
	if err != nil {
		return err
	}

	return nil
}

func renderGitlabCIFile(config *Config) error {
	name := ".gitlab-ci.yml"

	file, err := os.OpenFile(fmt.Sprintf("./%s", name), os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
	if err != nil {
		return err
	}

	defer file.Close()

	tmpl := template.Must(template.New(name + ".tmpl").ParseFiles(fmt.Sprintf("./src/%s.tmpl", name)))
	err = tmpl.Execute(file, *config)
	if err != nil {
		return err
	}

	return nil
}
